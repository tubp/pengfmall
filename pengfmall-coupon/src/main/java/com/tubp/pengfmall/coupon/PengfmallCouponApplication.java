package com.tubp.pengfmall.coupon;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PengfmallCouponApplication {

	public static void main(String[] args) {
		SpringApplication.run(PengfmallCouponApplication.class, args);
	}

}
