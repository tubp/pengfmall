package com.tubp.pengfmall.member;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PengfmallMemberApplication {

	public static void main(String[] args) {
		SpringApplication.run(PengfmallMemberApplication.class, args);
	}

}
