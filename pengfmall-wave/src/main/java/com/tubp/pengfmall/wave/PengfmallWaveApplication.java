package com.tubp.pengfmall.wave;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PengfmallWaveApplication {

	public static void main(String[] args) {
		SpringApplication.run(PengfmallWaveApplication.class, args);
	}

}
