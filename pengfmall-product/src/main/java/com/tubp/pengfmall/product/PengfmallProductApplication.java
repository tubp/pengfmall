package com.tubp.pengfmall.product;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PengfmallProductApplication {

	public static void main(String[] args) {
		SpringApplication.run(PengfmallProductApplication.class, args);
	}

}
